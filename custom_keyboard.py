from tkinter import *
import pyautogui
import keyboard

line_first = "qwertyuiop[]"
line_second = "asdfghjkl;'"
line_third = "zxcvbnm,./"
window = Tk()
window.title('Keyboard')
window.geometry('1000x400')
entry = Entry(window, width=15, bg='black', fg='white')
entry.pack()

keys = {}
panda = PhotoImage(file='panda.png').subsample(41)


def press(letter):
    pyautogui.write(letter)
    keyboard.write(letter)


x = 50
for letter in line_first:
    btn = Button(window, text=letter, bg='orange', bd=5, font=('', 15))
    btn.config(command=lambda b=letter: press(b))
    btn.place(x=x, y=50, width=50, height=50)
    keys[letter] = btn
    x += 50

x = 75
for letter in line_second:
    btn = Button(window, text=letter, bg='orange', bd=5, font=('', 15))
    btn.config(command=lambda b=letter: press(b))
    btn.place(x=x, y=100, width=50, height=50)
    keys[letter] = btn
    x += 50

x = 100
for letter in line_third:
    btn = Button(window, text=letter, bg='orange', bd=5, font=('', 15))
    btn.config(command=lambda b=letter: press(b))
    btn.place(x=x, y=150, width=50, height=50)
    keys[letter] = btn
    x += 50

keys['w'].config(image=panda)
keys['s'].config(bg = 'red')
window.mainloop()
